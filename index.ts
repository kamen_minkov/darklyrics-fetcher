import Track = require('./Track');

declare function require(name : string);

var x = require('x-ray')();
var http = require('http');

var artist : string = "entombed";
var album : string = "left hand path";

var searchURL : string = `http://www.darklyrics.com/search?q=${artist.replace(/\s/g, '+')}+${album.replace(/\s/g, '+')}`;

getSearchResult(searchURL)

function getSearchResult(url : string) {
	x(url, 'div.sen', 'a@href')((err, albumURL) => {
		if (!albumURL) {
			console.log("No results found");
		}
		else {
			getAlbumPage(albumURL);
		}
	});
}

function getAlbumPage(url : string) {
	x(url, 'div.lyrics@text')
	((err, data) => {
		stripAndGetLyrics(data);
	})
}

function stripAndGetLyrics(input : string) {
	var splitter : RegExp = /\d+[.].+$/gim;

	var titlesRaw = input.match(splitter);
	var lyricsRaw : Array<string> = input.split(splitter);
	lyricsRaw.splice(0, 1);

	var tracks : Array<Track> = [];

	for (let i = 0; i < titlesRaw.length; i++) {
		tracks.push(new Track(titlesRaw[i].replace(/\d+[.](.+)$/gim, '$1').trim()));
		tracks[i].lyrics = lyricsRaw[i];

	}
	var thanksSplitter : RegExp = /thanks to.+for/gim;

	if (tracks[tracks.length - 1].lyrics.match(thanksSplitter)) {
		tracks[tracks.length - 1].lyrics = tracks[tracks.length - 1].lyrics.split(thanksSplitter, 1)[0].trim();
	}

	console.log(tracks);
}
