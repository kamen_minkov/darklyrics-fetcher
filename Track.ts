class Track {
	artist : string;
	title : string;
	lyrics : string;

	constructor(title : string) {
		this.title = title;
	}
}

export = Track;
